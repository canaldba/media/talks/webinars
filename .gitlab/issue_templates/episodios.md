iDía: 7 de Abril 
Horario: 17:30 PM (ARG/UY)




## Formato

|Bloque|Tiempo|Observaciones
|------|------|-------------
|Intro|10|Comprende: Cortina, Intro, Sponsor y News
|Bloque 1|20|
|Inter|2|Presentación del siguiente bloque y Sponsor
|Bloque 2|20|  
|Ending|5| Wrap-up


## Información y Contenidos


Evento en el Calendar:

<a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=NW1vcjdtMGpibzRvbmJkcnIyZHVmaHI4ZmwgaWUxdmJnazA3amdodjB2bTc2cHFuaXY5czRAZw&amp;tmsrc=ie1vbgk07jghv0vm76pqniv9s4%40group.calendar.google.com"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>

### Intro

Presentar CanalDBA y nuevos recursos en Github y Gilab.

### Bloque 1

Deliver: 
Links:
- https://gitlab.com/canaldba/labs/dockerlab

### Bloque 2

Deliver: 
Links:
- https://github.com/datacharmer/dbdeployer

### Ending


## Otros

Video embedded:

```
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Cw1Nngpnlyo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```

Para agregar en el site (Calendario):

```
<iframe src="https://calendar.google.com/calendar/embed?src=ie1vbgk07jghv0vm76pqniv9s4%40group.calendar.google.com&ctz=America%2FArgentina%2FCordoba" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
```

[Link Calendario](https://calendar.google.com/calendar/embed?src=ie1vbgk07jghv0vm76pqniv9s4%40group.calendar.google.com&ctz=America%2FArgentina%2FCordoba)

## Sponsor

![OnGres](https://www.ongres.com/img/logo-ongres.svg "OnGres")
